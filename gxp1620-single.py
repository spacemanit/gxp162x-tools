#import xml.etree.ElementTree as ET

import lxml.etree as ET

print('Insert extension:')
klapka = input()

print('Input MAC address:')
MAC = input()

print('Input password:')
password = input()

filename = "GXP1620.xml"
outputfile = "cfg"+(MAC)+".xml"

xmlTree = ET.parse(filename)
rootElement = xmlTree.getroot()

rootElement.find("mac").text = (MAC.upper())

for element in rootElement.findall("config"):
    
        element.find('P270').text = (klapka)
        element.find('P35').text = (klapka)
        element.find('P36').text = (klapka)
        element.find('P3').text = (klapka)
        element.find('P34').text = (password)

#Write the modified xml file.        
xmlTree.write(outputfile,encoding='UTF-8',xml_declaration=True)
