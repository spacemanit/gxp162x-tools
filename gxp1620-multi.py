# Handling CSV in python
# https://pythonprogramming.net/reading-csv-files-python-3/
# https://thispointer.com/python-read-a-csv-file-line-by-line-with-or-without-header/

#import xml.etree.ElementTree as ET

from csv import reader
import lxml.etree as ET

# xml vzor stačí raz
filename = "GXP1620.xml"
xmlTree = ET.parse(filename)
rootElement = xmlTree.getroot()

with open('extensions.csv', 'r') as read_obj:
    csv_reader = reader(read_obj)
    # prechadzame riadky, čo riadok to nový xml file
    for row in csv_reader:
        #print(row[1], row[2], row[3])
        klapka = row[1]
        MAC = row[2]
        password = row[3]

        outputfile = "cfg"+(MAC.lower())+".xml" # outfile name je podľa mac adresy
        rootElement.find("mac").text = (MAC.upper())

        # prepis elementov ktoré nás zaujímajú
        for element in rootElement.findall("config"):
            element.find('P270').text = (klapka)
            element.find('P35').text = (klapka)
            element.find('P36').text = (klapka)
            element.find('P3').text = (klapka)
            element.find('P34').text = (password)

        # ulož ako nový file
        xmlTree.write(outputfile,encoding='UTF-8',xml_declaration=True)
